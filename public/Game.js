class GameClass {
	socket = io();
	players = []
	me = null
	hand = []
	turnType = null
	returnedCard = null
	isPlaying = false
	round = 0
	
	init(){
		this.socket.on('validate response',(response) => {
			if(response.error){
				alert(response.error)
				return
			}
			const turnScore = document.querySelector('#currentScore')
			turnScore.innerText = response.turnScore
		})

		this.socket.on('return response',(hand) => {
			console.log('1 hand', hand)
			this.renderDeck(hand, 'return')
		})

		this.socket.on('pick response',({ hand,currentPlayer, round }) => {
			this.renderDeck(hand, 'picking')
			this.validateTurn(currentPlayer, round, 'throw')
		})

		this.socket.on('round', ({ currentPlayer, round }) => {
				this.currentPlayer = currentPlayer
				this.validateTurn(currentPlayer, round, 'pick')
		})

		this.socket.on('you',(player) => {
			this.me  = player
		})

		this.socket.on('game started',({ players, currentPlayer, round }) => {
			const gameDeck = document.querySelector('#gameDeck')
			
			for(let i of players){
				const container = document.createElement('div')
				const label = document.createElement('span')
				label.innerText = i.name + "'s hand:"
				container.id = i.id + "_hand"
				container.appendChild(label)
				gameDeck.appendChild(container)
			}

			//Add turn scoreboard
			const turnScore = document.createElement('div')
			turnScore.id = 'turnScore'
			const currentScore = document.createElement('span')
			currentScore.id = 'currentScore'
			turnScore.appendChild(currentScore)
			gameDeck.appendChild(turnScore)

			//Add dropbox
			const dropbox = document.createElement('div')
			dropbox.id = 'dropbox'
			gameDeck.appendChild(dropbox)

			//Apply buttons
			const returnButton = document.createElement('button')
			const pickButton = document.createElement('button')
			const handButton = document.createElement('button')
			returnButton.innerText = 'Return card'
			pickButton.innerText = 'Pick card'
			handButton.innerText = 'Submit hand'
			returnButton.disabled = true
			pickButton.disabled = true
			handButton.disabled = true
			returnButton.id = 'returnButton'
			pickButton.id = 'pickButton'
			handButton.id = 'handButton'
			handButton.addEventListener('click', this.validateHand)
			pickButton.addEventListener('click', this.pickCard)
			returnButton.addEventListener('click', this.returnCard)
			gameDeck.appendChild(returnButton)
			gameDeck.appendChild(pickButton)
			gameDeck.appendChild(handButton)

			//Add my deck
			const myHand = document.createElement('div')
			myHand.id = 'myHand'
			gameDeck.appendChild(myHand)
			this.validateTurn(currentPlayer, round, 'throw')					
		})

		this.socket.on('new player', function(name){
			console.log('new player',name)
		})

		this.socket.on('get hand',(hand) => {
			this.renderDeck(hand, false)
		})
		
		//Prompt
		const name = prompt("Fill your name pls")
		this.socket.emit('set player', name)
	}

	allowDrag = () => {
		if(this.turnType === 'pick' || this.round === 0){
			console.log('nene')
			return
		}
		const hand = document.querySelectorAll('.hand-card')
		const dropbox = document.querySelector('#dropbox')

		dropbox.addEventListener('drop',function(event){
			const submitButton = document.querySelector('#handButton')
			const dropbox = document.querySelector("#dropbox")
			const sourceId = event.dataTransfer.getData("text")
			const image = document.getElementById(sourceId)
			image.style.opacity = 0.2
			image.draggable = false

			const newImage = document.createElement('img')
			newImage.src = image.src
			newImage.id = sourceId + "_dragged"
			newImage.style.width = '75px'
			newImage.style.height = `${75 / 0.6543560606}px`
			newImage.addEventListener('click',(ev)=>{
				ev.currentTarget.remove()
				const backCard = document.getElementById(sourceId)
				backCard.draggable = true
				backCard.style.opacity = 1
			})
			dropbox.appendChild(newImage)

			if(dropbox.childElementCount > 2){
				submitButton.disabled = false
			}

		});

		dropbox.addEventListener('dragenter',function(event){
				event.preventDefault();
		})

		dropbox.addEventListener('dragover',function(event){
				event.preventDefault();
		})

		for(let card of hand){
			card.draggable = true
			card.addEventListener('dragstart',(event) => {
				event.dataTransfer.setData("text/plain", event.target.id);
			})
		}
	}

	onDeckCard = (event) => {
			if(this.turnType === 'throw'){
				const picked = document.querySelector('.picked-throw')
				if(picked){
					picked.classList.remove('picked-throw')
				}
				event.currentTarget.classList.add('picked-throw')
				this.returnedCard = event.currentTarget.id
				this.blockButton('returnButton', false)
			}
	}

	renderDeck = (hand, reason) =>{
		this.hand = hand
		let pickedId = null
		const gameDeck = document.querySelector('#gameDeck')
		const myHand = document.querySelector('#myHand')
		console.log('2 hand', hand,reason)

		if(reason){
			myHand.innerHTML = ''
			if(reason === 'picking'){
				pickedId = JSON.stringify(hand.slice(-1)[0])
				this.blockButton('pickButton',true)
			}
			if(reason === 'return'){
			
			}
		}

		for(let card of this.hand){
			const image = document.createElement('img')
			image.src = `/JPEG/${String(card.symbol).toLocaleUpperCase()}${card.type.substring(0, 1).toUpperCase()}.jpg`
			image.style.width = '100px'
			image.style.height = `${100 / 0.6543560606}px`
			image.style.border = '2px solid transparent'
			image.style.boxSizing = 'border-box'
			image.classList.add('hand-card')
			image.id = JSON.stringify(card)
			if(pickedId === JSON.stringify(card)){
				image.classList.add('picked-pick')
			}
			image.addEventListener('click', this.onDeckCard)
			myHand.appendChild(image)
			gameDeck.appendChild(myHand)
		}

	}

	validateTurn = (currentPlayer, round, turnType) => {
		this.round = round
		this.turnType = turnType
	
		if(this.me.id === currentPlayer){
			if(turnType === 'throw'){
				this.blockButton('pickButton', true)
				this.blockButton('handButton', round === 0 ? true : false)
			}
			if(turnType === 'pick'){
				this.blockButton('pickButton', false)
				this.blockButton('returnButton', true)
				this.blockButton('handButton', false)
			}
			//TODO stop draggable when not your round
			this.allowDrag()
		} else {
			this.blockButton('pickButton', true)
			this.blockButton('handButton', true)
			this.blockButton('returnButton', true)
			this.turnType = null
		}

	}

	pickCard = () => {
		this.socket.emit('pick hand')
	}

	returnCard = () => {
		this.socket.emit('return hand', this.returnedCard)
	}

	validateHand = () => {
		const dropbox = document.querySelector('#dropbox')
		const items = []
		
		for(let card of dropbox.children){
			const parsed = JSON.parse(card.id.replace('_dragged',""))
			items.push(parsed)		
		}

		this.socket.emit('validate hand', items)
	}

	blockButton = (id, block) => {
		const button  = document.querySelector(`#${id}`)
		button.disabled = block
	}
}

window.onload = () => {
	const Game = new GameClass()
	Game.init()
}