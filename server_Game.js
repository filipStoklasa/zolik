const shuffle = require('lodash/shuffle')
const testField = [
	{ symbol:2, type:"club", value:2, index:1 },
	{ symbol:3, type:"club", value:3, index:2 },
	{ symbol:4, type:"club", value:4, index:3 },
	{ symbol:5, type:"club", value:5, index:4 },
	{ symbol:6, type:"club", value:6, index:5 },
	{ symbol:7, type:"club", value:7, index:6 },
	{ symbol:8, type:"club", value:8, index:7 },
	{ symbol:9, type:"club", value:9, index:8 },
	{ symbol:10, type:"club", value:10, index:9 },
	{ symbol:'j', type:"club", value:10, index:10 },
	{ symbol:'q', type:"club", value:10, index:11 },
	{ symbol:'k', type:"club", value:10, index:12},
	{ symbol:'a', type:"club", value:10, index:13 },
	{ symbol:'q', type:"diamond", value:10, index:14 },
	{ symbol:'q', type:"club", value:10, index:15 },
]

const controls = [
	"ac1c2c3c4c5c6c7c8c9c10cjcqckcac",
	"ah1h2h3h4h5h6h7h8h9h10hjhqhkhah",
	"as1s2s3s4s5s6s7s8s9s10sjsqsksas",
	"ad1d2d3d4d5d6d7d8d9d10djdqdkdad",
]

class GameClass {
	round = 0
	players = []
	currentPlayer = null
	started = false
	cardPack = []
	playersHands = {}
	folded = []
	gamesOnDeck = {}
//TODO middleware to check if the right player took turn
  prepareCards() {
		let pack = []
    const symbols = [ 2, 3, 4, 5, 6, 7, 8, 9, 10, 'j', 'q', 'k', 'a']
    const types = ['club', 'heart', 'spade', 'diamond']

    for(let type of types){
      for(let symbol of symbols){
				let value = symbol
        if(typeof symbol === 'string'){
            value = 10
        }
        pack.push({ symbol, type, value })
      }
		}
		this.cardPack =  shuffle([...pack,...pack].map((item,index)=>({...item, index})))
	}

	addPlayer(player){
		this.players.push(player)
	}

	getPlayer(id){
		return this.players.find((player) => player.id === id)
	}

	getHand(id){
		return this.playersHands[id]
	}

	pickCard = (id) => {
		const card = this.cardPack.pop()
		this.playersHands[id].push(card)
		return this.playersHands[id]
	}

	returnCard = (id, card) => {
		const parsed = JSON.parse(card)
		this.playersHands[id] = this.playersHands[id].filter(localCard=>localCard.index !== parsed.index)
		this.folded.push(card)
		this.nextPlayer()
		this.started = false
		this.round += 1
		return this.playersHands[id]
	}

	nextPlayer = () => {
		const playersMap = this.players.map(player => player.id)
		this.currentPlayer = this.players[(playersMap.indexOf(this.currentPlayer) + 1) % playersMap.length].id
	}

	giveCards(){
		//Todo 
		for(let i in this.players){
			this.playersHands[this.players[i].id] = []
			for(let j=0;j<15;j++){
				if(Number(i) === this.players.length -1 && j === 14){
					break
				}
				const card = this.cardPack.pop()
				this.playersHands[this.players[i].id] = [...(	this.playersHands[this.players[i].id] || []), card]
				// this.playersHands[this.players[i].id] = testField
			}
		}
	}

	startGame(){
		this.prepareCards()
		console.log(this.players[0].id )
		this.currentPlayer = this.players[0].id 
		this.started = true
		this.giveCards()
	}

	validateHand = (id, hand) => {
		//TODO validate if cards belong to player
		let straight = false
		let same = false
		let score = 0
		let controlString = ''
		
		for(let card of hand){
			score += card.value
			controlString += `${String(card.symbol).substring(0,1)}${card.type.substring(0,1)}`
		}

		for(let control of controls){
			if(control.indexOf(controlString) !== -1){
				straight = true
			}
		}
		if(!this.gamesOnDeck.hasOwnProperty(id) && !straight){
			return {
				error: 'Has to start with straight'
			}
		}
		
		this.gamesOnDeck[id] = [...(this.gamesOnDeck[id] || []), hand]
		return {
			turnScore: score
		}
	}

	returnState(){
		return {
			players: this.players,
			gamesOnDeck: this.gamesOnDeck,
			currentPlayer: this.currentPlayer,
			folded: this.folded.slice(-1)[0],
			round: this.round
		}
	}

}

module.exports = new GameClass()