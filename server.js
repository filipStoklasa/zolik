const Game = require('./server_Game')
const express = require('express')
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);



app.use(express.static('public'))

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

io.on('connection', (socket) => {
	socket.on('set player', (name) => {
			Game.addPlayer({ id: socket.id, name })
			socket.broadcast.emit('new player', Game.returnState())
			socket.emit('you', Game.getPlayer(socket.id))
			if(Game.players.length === 2){
				Game.startGame()
				io.emit('game started', Game.returnState())
				for(let player of Game.players){
					io.to(player.id).emit('get hand', Game.getHand(player.id))
				}
			}
	})
	socket.on('validate hand',(hand)=>{
		const result = Game.validateHand(socket.id, hand)
		socket.emit('validate response',result)
	})

	socket.on('pick hand',() => {
		const result = Game.pickCard(socket.id)
		socket.emit('pick response', { hand: result, ...Game.returnState() })
	})

	socket.on('return hand', (card) => {
		const result = Game.returnCard(socket.id, card)
		console.log(result)
		socket.emit('return response', result)
		io.emit('round', Game.returnState())
	})
});

http.listen(3000, () => {
  console.log('listening on *:3000');
});